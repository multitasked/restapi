# запускает uvicorn и использует loguru для логирования

import os
import sys
import logging

from loguru import logger
from uvicorn import Config, Server
from pathlib import Path


APP = 'rest_api'
HOST = '127.0.0.1'
PORT = 8000
LOG_LEVEL = logging.getLevelName(os.environ.get("LOG_LEVEL", "ERROR"))


# Cериализация журнала в JSON
JSON_LOGS = True
LOG_FILE = Path.cwd().as_posix() + '/logs/logs.json'


class InterceptHandler(logging.Handler):
    """
    Этот код скопирован из документации Loguru.
    Этот обработчик будет использоваться для перехвата логов,
    выдаваемых библиотеками, и повторной выдачи их через Loguru.
    """
    def emit(self, record):
        # получить соответствующий уровень Логуру, если он существует
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # найти абонента, от которого поступило зарегистрированное сообщение
        frame, depth = sys._getframe(6), 6
        while frame and frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


def setup_logging():
    # перехватывать все в корневом регистраторе
    logging.root.handlers = [InterceptHandler()]
    logging.root.setLevel(LOG_LEVEL)

    # удалить обработчики всех остальных регистраторов
    # и распространяются на корневой регистратор
    for name in logging.root.manager.loggerDict.keys():
        logging.getLogger(name).handlers = []
        logging.getLogger(name).propagate = True

    # настройка loguru
    logger.configure(handlers=[{"sink": LOG_FILE, "serialize": JSON_LOGS, "level": LOG_LEVEL}])


if __name__ == '__main__':
    server = Server(
        Config(
            f"{APP}:app",
            host=HOST,
            port=PORT,
        ),
    )

    setup_logging()
    server.run()