from faker import Faker
from uuid import uuid4
from json import dumps
from time import time
from argparse import ArgumentParser
from requests import post
from loguru import logger


logger.add('generator_fake.log', format='{time} {level} {message}', level="ERROR", rotation="1 week")
STRINGS_LIST = ["EXTRAMURAL", "FULL_TIME", "PART_TIME", "SHORT_EXTRAMURAL", "SHORT_FULL_TIME", "EXTERNAL"]


@logger.catch
def main():
    args = get_args()                                   # получаем аргументы командной строки для генерации данных
    fake_data = gen_fake_data(args.schema, args.size)   # генерация данные
    # print_fake_data(fake_data)                        # вывод сгенерированных данных
    send_data(args.schema, fake_data)                   # отправка сгенерированных данных


def get_args():
    parser = ArgumentParser()
    parser.add_argument('--schema', choices=['up', 'edu_prog'], required=True, help='Схема для генерации данных')
    parser.add_argument('--size', type=int, required=True, help='Количество генерируемых данных')
    return parser.parse_args()


def gen_fake_data(schema: str, num_itr: int):
    fake_data = []

    Faker.seed(time())
    fake = Faker(locale='ru_RU')

    if schema == 'up':
        for _ in range(num_itr):
            fake_data = generation_data_up(fake, fake_data)
        fake_data = {
            'organization_id': str(uuid4()),
            'up': fake_data
        }
    elif schema == 'edu_prog':
        for _ in range(num_itr):
            fake_data = generation_data_edu_prog(fake, fake_data)
        fake_data = {
            'organization_id': str(uuid4()),
            'edu_prog': fake_data
        }

    return fake_data


def generation_data_edu_prog(fake, fake_data: list):
    fake_data.append({
        'external_id': fake.pyint(min_value=100000, max_value=999999),
        'title': fake.word(),
        'direction': fake.word(),
        'code_direction': fake.phone_number(),
        'start_year': int(fake.date(pattern='%Y')),
        'end_year': int(fake.date(pattern='%Y'))
    })
    return fake_data


def generation_data_up(fake, fake_data: list):
    fake_data.append({
        'external_id': str(uuid4()),
        'title': fake.word(),
        'direction': fake.word(),
        'code_direction': fake.phone_number(),
        'start_year': int(fake.date(pattern='%Y')),
        'end_year': int(fake.date(pattern='%Y')),
        'education_form': generation_data_education_form(),
        'education_program': str(uuid4())
    })
    return fake_data


def generation_data_education_form():
    rt = random_time(len(STRINGS_LIST))
    return STRINGS_LIST[rt]


def random_time(lsl: int):
    time_ = time()
    random_num = int(time_ % lsl)
    return random_num


def print_data(data):
    json_string = dumps(data, indent=2, ensure_ascii=False).encode('utf8')
    print(json_string.decode('utf8'))


def send_data(schema: str, data: list):
    url = 'http://127.0.0.1:8000'

    if schema == 'up':
        url += '/up'
    elif schema == 'edu_prog':
        url += '/edu_prog'

    response = post(url=url, json=data)
    print("response.status_code --- {0}".format(response.status_code))


if __name__ == '__main__':
    main()
