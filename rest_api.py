from fastapi import FastAPI
from validations import ClassJsonUp, ClassJsonEduProg
from os import system


app = FastAPI(
    title='RestAPI'
)
data = {}


# INIT

@app.on_event('startup')
def init_data():
    data['edu_prog'] = []
    data['up'] = []


# GET

@app.get("/is_alive")
def get_is_alive():
    return True


@app.get("/edu_prog")
def get_edu_prog(size_: int):
    cmd = 'python generator_fake.py --schema edu_prog --size ' + str(size_)
    system(cmd)
    return {"status": 200, "data": data['edu_prog']}


@app.get("/up")
def get_up(size_: int):
    cmd = 'python generator_fake.py --schema up --size ' + str(size_)
    system(cmd)
    return {"status": 200, "data": data['up']}


# POST

@app.post("/edu_prog")
def post_edu_prog(message: ClassJsonEduProg):
    data['edu_prog'] = message.edu_prog
    return {"status": 200, "data": data['edu_prog']}


@app.post("/up")
def post_up(message: ClassJsonUp):
    data['up'] = message.up
    return {"status": 200, "data": data['up']}
