# Запуск проекта

1. Установка менеджера пакетов poetry (аналог pip) 
   ```pip install poetry```
2. Установка зависимостей (из файла pyproject.toml) командой 
   ```poetry install```
3. Запуск сервера RestAPI ```python run_uvicorn.py```
4. Запуск микросервиса ```python generator_fake.py --schema edu_prog --size 2```
5. Страница для тестирования http://127.0.0.1:8000/docs#/
