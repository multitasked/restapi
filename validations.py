from pydantic import BaseModel, Field
from typing import Literal
from datetime import datetime


NOW_YEAR = datetime.now().year
UUID4_STR = "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89abAB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}"
NUMBERS_STR = "^\+?[\(\)\-\d\s]+$"
STRINGS_LIST = Literal["EXTRAMURAL", "FULL_TIME", "PART_TIME", "SHORT_EXTRAMURAL", "SHORT_FULL_TIME", "EXTERNAL"]


class ClassEduProg(BaseModel):
    external_id: str = Field(..., min_length=6, max_length=6, regex=NUMBERS_STR)
    title: str = Field(..., min_length=1)
    direction: str = Field(..., min_length=1)
    code_direction: str = Field(..., min_length=1, max_length=20, regex=NUMBERS_STR)
    start_year: int = Field(0, ge=1900, le=NOW_YEAR)
    end_year: int = Field(0, ge=1900, le=NOW_YEAR)


class ClassUp(BaseModel):
    external_id: str = Field(..., min_length=1, regex=UUID4_STR)
    title: str = Field(..., min_length=1)
    direction: str = Field(..., min_length=1)
    code_direction: str = Field(..., min_length=1, max_length=20, regex=NUMBERS_STR)
    start_year: int = Field(0, ge=1900, le=NOW_YEAR)
    end_year: int = Field(0, ge=1900, le=NOW_YEAR)
    education_form: STRINGS_LIST
    education_program: str = Field(..., min_length=1, regex=UUID4_STR)


class ClassJsonUp(BaseModel):
    organization_id: str = Field(..., min_length=1, regex=UUID4_STR)
    up: list[ClassUp]


class ClassJsonEduProg(BaseModel):
    organization_id: str = Field(..., min_length=1, regex=UUID4_STR)
    edu_prog:  list[ClassEduProg]
